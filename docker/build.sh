#!/usr/bin/env bash

set -x

mkdir --parents tmp/src/
cp --recursive ../src tmp/src/

IMAGE_NAME=print_my_messages
REGISTRY_URL=registry.gitlab.com/hellodeargrandma
TAG=${REGISTRY_URL}/${IMAGE_NAME}:latest

docker buildx build \
    --platform linux/arm/v6,linux/amd64 \
    --tag ${TAG} \
    .
docker push ${TAG}

rm -rf tmp

