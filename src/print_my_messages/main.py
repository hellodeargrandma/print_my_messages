#!/usr/bin/env python3

import os
import asyncio
import logging
import time
import argparse
from pathlib import Path

from print_my_messages.printer import Printer, get_printer
from print_my_messages.telegram_bot import BotWithPrinter, start_polling

tgm_bot_token = "1880806140:AAEft7iHSB5qLi77vhEbHyPsOo_d89qKqbA"

logging.basicConfig(level=logging.DEBUG)
logging.getLogger("aiogram").setLevel(logging.WARNING)


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Telegram Bot that prints messages on thermal printer."
    )
    parser.add_argument(
        "-p", "--printer", help="Path to printer device.", default="/dev/ttyACM0"
    )

    return parser.parse_args()


def main() -> None:
    args = parse_arguments()

    printer = get_printer(args.printer)

    dp = BotWithPrinter(tgm_bot_token, printer)
    start_polling(dp)


if __name__ == "__main__":
    main()
