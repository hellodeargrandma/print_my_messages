import logging
import time
from datetime import datetime

from escpos.printer import Serial


class Printer(Serial):
    def __init__(self, *args, **kwargs):
        super(Printer, self).__init__(*args, **kwargs)

    async def print_message(self, message):
        self.set(align="left")
        user = message["from"]
        date = message["date"]
        self.text(f"From {user.first_name} {user.last_name} ({date.isoformat(' ')}):\n")

        if message.text:
            self.text(f"{message.text}\n")

        if message.photo:
            try:
                await self._print_photo(message)
            except Exception as exc:
                logging.debug(f"exception: {exc}")
                raise

    async def _get_best_res_photo(self, message):
        max_width = 512
        max_height = 1100
        # Lookup for biggest variants of photos which are less than max_width px
        # wide and max_height long.
        best_photo = message.photo[0]
        for photo in message.photo:
            photo_id = photo["file_unique_id"]
            logging.debug(
                f"Photo_id: {photo_id}; photo size: {photo['width']}x{photo['height']}"
            )

            if photo["width"] > max_width or photo["height"] > max_height:
                continue

            if (
                best_photo["width"] > max_width
                or best_photo["width"] < photo["width"]
                or best_photo["height"] > max_height
                or best_photo["height"] < photo["height"]
            ):
                best_photo = photo
            logging.debug(
                f"new best size: {best_photo['width']}x{best_photo['height']}"
            )

        return best_photo

    async def _print_photo(self, message):
        logging.debug(f"message: {message}")

        best_photo = await self._get_best_res_photo(message)

        logging.debug(f"photo: {best_photo}")
        file_info = await best_photo.get_file()
        file = await best_photo.download()
        logging.debug(f"file_info: {file_info}")

        self.text(f"\n")
        time.sleep(1)
        self.image(file_info.file_path)
        self.text("\n")

    def print_bottom(self) -> None:
        self.set(align="center")
        self.text("------------------------------------------\n")
        self.text("Print message to telegram bot:\n")
        self.qr("https://t.me/hdg_messages_printing_bot")
        self.cut()


def get_printer(dev_file: str = "/dev/ttyACM0") -> Printer:
    printer = Printer(
        devfile=dev_file,
        baudrate=115200,
        bytesize=8,
        parity="N",
        stopbits=1,
        timeout=1.00,
        dsrdtr=True,
    )
    printer.set(align="left")

    return printer
