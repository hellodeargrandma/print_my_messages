#!/usr/bin/env python3

import argparse
from escpos.printer import Dummy

from print_my_messages.printer import get_printer


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Small printer to test printing images on termal printer."
    )
    parser.add_argument("-i", "--image", help="Path to image to print.", required=True)
    parser.add_argument(
        "-p", "--printer", help="Path to printer device.", default="/dev/ttyACM0"
    )

    return parser.parse_args()


def main():
    args = parse_arguments()
    printer = get_printer()
    d = Dummy()
    d.text("\n")
    d.text("Test\n")
    d.text("\n")
    d.image(args.image)
    d.text("\n")
    d.cut()
    printer._raw(d.output)


if __name__ == "__main__":
    main()
