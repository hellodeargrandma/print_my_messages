#!/usr/bin/env python3

import os
import asyncio
import logging
import time
import argparse
from aiogram import Bot, Dispatcher, executor, types
from pathlib import Path

from print_my_messages.printer import Printer


class BotWithPrinter(Dispatcher):
    def __init__(self, bot_token: str, printer: Printer):
        bot = Bot(token=bot_token)
        super(BotWithPrinter, self).__init__(bot)

        self.printer = printer

        @self.message_handler(
            content_types=[types.ContentType.TEXT, types.ContentType.PHOTO]
        )
        async def print(message: types.Message):
            await self.printer.print_message(message)
            self.printer.print_bottom()


def start_polling(dp) -> None:
    executor.start_polling(dp, skip_updates=True)


def parse_arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Telegram Bot that prints messages on thermal printer."
    )
    parser.add_argument(
        "-p", "--printer", help="Path to printer device.", default="/dev/ttyACM0"
    )

    return parser.parse_args()
